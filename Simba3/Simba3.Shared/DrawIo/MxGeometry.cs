﻿using System.Xml.Serialization;

namespace Simba3.Shared.DrawIo;

[XmlRoot(ElementName = "mxGeometry")]
public class MxGeometry
{
    [XmlAttribute(AttributeName = "x")]
    public string X { get; set; }

    [XmlAttribute(AttributeName = "y")]
    public string Y { get; set; }

    [XmlAttribute(AttributeName = "width")]
    public string Width { get; set; }

    [XmlAttribute(AttributeName = "height")]
    public string Height { get; set; }

    [XmlAttribute(AttributeName = "as")]
    public string As { get; set; }

    [XmlElement(ElementName = "mxPoint")]
    public List<MxPoint> MxPoint { get; set; }

    [XmlAttribute(AttributeName = "relative")]
    public string Relative { get; set; }
}
