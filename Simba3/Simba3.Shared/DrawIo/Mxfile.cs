﻿using System.Xml.Serialization;

namespace Simba3.Shared.DrawIo;

[XmlRoot(ElementName = "mxfile")]
public class Mxfile
{
    [XmlElement(ElementName = "diagram")]
    public Diagram Diagram { get; set; }

    [XmlAttribute(AttributeName = "host")]
    public string Host { get; set; }

    [XmlAttribute(AttributeName = "agent")]
    public string Agent { get; set; }

    [XmlAttribute(AttributeName = "version")]
    public string Version { get; set; }
}
