﻿using System.Xml.Serialization;

namespace Simba3.Shared.DrawIo;

[XmlRoot(ElementName = "root")]
public class Root
{
    [XmlElement(ElementName = "mxCell")]
    public List<MxCell> MxCell { get; set; }
}
