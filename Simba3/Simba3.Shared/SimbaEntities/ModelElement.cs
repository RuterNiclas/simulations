﻿using System.Drawing;
using System.Numerics;

namespace Simba3.Shared.SimbaEntities
{
    public abstract class ModelElement
    {
        public string Id { get; set; }
    }
}
