﻿using System.Xml;
using System.Xml.Serialization;

namespace Simba3.WebClient.Models.DrawIo
{
    public static class DrawIoParser
    {
        public static Mxfile ParseFile(string path)
        {
            if (File.Exists(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Mxfile));
                var mxfile = (Mxfile)serializer.Deserialize(new XmlTextReader(path));
            }
            return null;
        }
    }
}
