﻿using System.Xml.Serialization;

namespace Simba3.WebClient.Models.DrawIo;

[XmlRoot(ElementName = "root")]
public class Root
{
    [XmlElement(ElementName = "mxCell")]
    public List<MxCell> MxCell { get; set; }
}
