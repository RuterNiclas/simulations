﻿namespace Simba3.WebClient.Shared;

public class UiEnumHelper<T, E>
    where E : Enum
{
    private T handledObject;
    private Action<T, E> setHandledObjectEnumTarget;
    private Func<T, E, bool> matchHandledObjectWithEnum;

    private EnumWithName<E> enumWithName;

    public EnumWithName<E> EnumWithName
    {
        get { return enumWithName; }
        set
        {
            setHandledObjectEnumTarget.Invoke(handledObject, value.Enum);
            enumWithName = value;
        }
    }

    public List<EnumWithName<E>> EnumsWithNames = new List<EnumWithName<E>>();

    public UiEnumHelper(
        T handledObject,
        Action<T, E> setReflectedValueByEnum,
        Func<T, E, bool> matchReflectedObjectWithEnum
    )
    {
        this.handledObject = handledObject;
        setHandledObjectEnumTarget = setReflectedValueByEnum;
        matchHandledObjectWithEnum = matchReflectedObjectWithEnum;

        foreach (var item in Enum.GetValues(typeof(E)))
        {
            var enumValue = (E)item;
            EnumsWithNames.Add(
                new EnumWithName<E>() { Enum = enumValue, Name = enumValue.ToString() }
            );
        }

        enumWithName = EnumsWithNames.First();
    }

    public void AlignSelectedValue(T initialValue)
    {
        var matches = EnumsWithNames.Where(x =>
            matchHandledObjectWithEnum.Invoke(handledObject, x.Enum)
        );

        if (matches.Any())
        {
            EnumWithName = matches.First();
        }
        else
        {
            throw new Exception(
                $"{nameof(UiEnumHelper<T, E>)} got an initial reflectedValue not matchable"
            );
        }
    }
}
