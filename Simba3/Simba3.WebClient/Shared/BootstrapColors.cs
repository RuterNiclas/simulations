﻿namespace Simba3.WebClient.Shared;

public class BootstrapColors
{
    public string TextPrimary = "text-primary";
    public string TextSeconday = "text-secondary";
    public string TextSuccess = "text-success";
    public string TextDanger = "text-danger";
    public string TextWarning = "text-warning";
    public string TextInfo = "text-info";
    public string TextLightBgDark = "text-light bg-dark";
    public string TextDark = "text-dark";
    public string TextMuted = "text-muted";
    public string TextWhiteBgDark = "text-white bg-dark";
}
