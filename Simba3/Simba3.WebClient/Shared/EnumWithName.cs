﻿namespace Simba3.WebClient.Shared;

public class EnumWithName<T>
    where T : Enum
{
    public T? Enum { get; set; }
    public string Name { get; set; } = string.Empty;
}
