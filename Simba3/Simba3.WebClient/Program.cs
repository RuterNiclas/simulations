namespace Simba3.WebClient
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            await AppendWwwRootConfigFileAsync(builder);

            builder.Services.AddScoped<DialogService>();
            builder.Services.AddScoped<NotificationService>();
            builder.Services.AddScoped<TooltipService>();
            builder.Services.AddScoped<ContextMenuService>();

            builder.Services.AddScoped<DialogService>();
            builder.Services.AddBlazoredLocalStorage();

            var host = builder
            //.UseSerilog()
            .Build();

            await host.RunAsync();
        }

        /// <summary>
        /// Reference to functional description:
        /// https://adrianhall.github.io/asp.net/2022/09/03/blazor-wasm-aad-auth-part-3/
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        private static async Task AppendWwwRootConfigFileAsync(WebAssemblyHostBuilder builder)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress),
            };

            using var response = await httpClient.GetAsync("clientconfiguration.json");
            using var stream = await response.Content.ReadAsStreamAsync();
            builder.Configuration.AddJsonStream(stream);
        }
    }
}
