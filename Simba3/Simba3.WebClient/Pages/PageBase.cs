﻿namespace Simba3.WebClient.Pages;

public abstract class PageBase : ComponentBase
{
    // Row and column styles
    // https://www.w3schools.com/bootstrap4/bootstrap_grid_xlarge.asp
    protected string rowStyle = "margin-top: 10px";
    protected string keyColStyle = "col-sm-4 col-md-7 col-lg-5 col-xl-3";
    protected string valueColStyle = "col-sm-8 col-md-5 col-lg-7 col-xl-9";
    protected string marginBottom = ".mb-3";
}
