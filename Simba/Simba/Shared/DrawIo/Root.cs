﻿using System.Xml.Serialization;

namespace Simba.Shared.DrawIo;

[XmlRoot(ElementName = "root")]
public class Root
{
    [XmlElement(ElementName = "mxCell")]
    public List<MxCell> MxCell { get; set; }
}