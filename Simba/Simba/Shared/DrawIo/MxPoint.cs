﻿using System.Xml.Serialization;

namespace Simba.Shared.DrawIo;

[XmlRoot(ElementName = "mxPoint")]
public class MxPoint
{
    [XmlAttribute(AttributeName = "x")]
    public string X { get; set; }
    [XmlAttribute(AttributeName = "y")]
    public string Y { get; set; }
    [XmlAttribute(AttributeName = "as")]
    public string As { get; set; }
}