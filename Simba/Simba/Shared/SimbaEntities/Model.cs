﻿namespace Simba.Shared.SimbaEntities
{
    public class Model
    {
        public string Name { get; set; }
        public List<Page> Pages { get; set; } = new List<Page>();
    }
}
