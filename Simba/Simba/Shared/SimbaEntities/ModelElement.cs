﻿using System.Drawing;
using System.Numerics;

namespace Simba.Shared.SimbaEntities
{
    public abstract class ModelElement
    {
        public string Id { get; set; }
    }
}
