﻿namespace Simba.Client
{
    public static class Styles
    {
        // Row and column styles
        // https://www.w3schools.com/bootstrap4/bootstrap_grid_xlarge.asp
        public static string rowStyle = "margin-top: 10px";
        public static string keyColStyle = "col-sm-4 col-md-7 col-lg-5 col-xl-3";
        public static string valueColStyle = "col-sm-8 col-md-5 col-lg-7 col-xl-9";
        public static string marginBottom = ".mb-3";

    }
}
