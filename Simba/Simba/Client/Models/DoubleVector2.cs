﻿namespace Simba.Client.Models
{
    public class DoubleVector2
    {

        public double X { get; set; }
        public double Y { get; set; }

        public DoubleVector2(double x, double y) {  X = x; Y = y; }

        public double GetXAbsoulteLengthClampedBewlowOrEqual(double clampLength)
        {

            if(clampLength < 0)
            {
                throw new Exception("Clamp length was less than zero.");
            }

            if(X >= 0)
            {
                if(X > clampLength)
                {
                    return clampLength;
                }
                else
                { 
                    return X; 
                }
            }
            else
            {
                if(Math.Abs(X) < clampLength)
                {
                    return X;
                }
                else
                {
                    return -clampLength;
                }
            }

        }

        public double GetYAbsoulteLengthClampedBewlowOrEqual(double clampLength)
        {

			if (clampLength < 0)
			{
				throw new Exception("Clamp length was less than zero.");
			}

			if (Y >= 0)
            {
                if (Y > clampLength)
                {
                    return clampLength;
                }
                else
                {
                    return Y;
                }
            }
            else
            {
                if (Math.Abs(Y) < clampLength)
                {
                    return Y;
                }
                else
                {
					return -clampLength;
				}
            }
        }
    }
}
