﻿namespace Simba.Client.Models
{
    public class FloatVector2
    {
        public float X { get; set; }
        public float Y { get; set; }
    }
}
