﻿using System.Drawing;
using System.Transactions;

namespace Simba.Client.Models
{
    public class GraphicSize
    {

        private const double half = 0.5d;

        public int Width { get; set; }
        public int Height { get; set; }

        public Size Size { get { return new Size(Width, Height); } }

        public GraphicSize(Size size)
        {
            Width = size.Width;
            Height = size.Height;
        }

        public GraphicSize(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public double HalfWidth()
        {
            return Width * half;
        }

        public double HalfHeight()
        {
            return Height * half;
        }
    }
}
