﻿namespace Simba.Client.Models
{
    public class IntVector2
    {
        public int X { get; set; }
        public int Y { get; set; }

        public IntVector2(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
