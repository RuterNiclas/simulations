﻿using Simba.Shared.DrawIo;

namespace Simba.Client.Models.ModelHandler
{
    public class MxfileAndDirectory
    {
        private Mxfile Mxfile { get; set; }
        private string Path { get; set;}
    }
}
