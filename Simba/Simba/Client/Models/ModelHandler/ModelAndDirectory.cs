﻿using Simba.Shared.DrawIo;
using Simba.Shared.SimbaEntities;

namespace Simba.Client.Models.ModelHandler
{
    public class ModelAndDirectory
    {
        private Model Model { get; set; }
        private string Path { get; set; }
    }
}
