﻿

let widthPadding = 300;
let heightPadding = 100;

export function initRenderJS(instance) {
    window.theInstance = instance;

    window.addEventListener("resize", resizeCanvasToFitWindow);

    window.addEventListener("mousedown", mouseDown);
    window.addEventListener("mouseup", mouseUp);

    const canvasHolder = document.getElementById("canvasHolder");
    canvasHolder.addEventListener("mousemove", mouseMove);

    window.addEventListener("wheel", mouseWheel);

    resizeCanvasToFitWindow();

    window.requestAnimationFrame(renderJS);

    window.getCanvasHolderSize = () => {
        return true;
    };
}

function renderJS(timeStamp) {
    theInstance.invokeMethodAsync('RenderInBlazor', timeStamp);
    window.requestAnimationFrame(renderJS);
}

function resizeCanvasToFitWindow() {
    var holder = document.getElementById('canvasHolder');
    var canvas = holder.querySelector('canvas');
    if (canvas) {
        canvas.width = window.innerWidth - widthPadding;
        canvas.height = window.innerHeight - heightPadding;
        theInstance.invokeMethodAsync('ResizeInBlazor',
                                      canvas.width,
                                      canvas.height);
    }
}

function mouseDown(e) {
    var args = canvasMouseMoveArgs(e);
    theInstance.invokeMethodAsync('OnMouseDown', args);
}

function mouseUp(e) {
    var args = canvasMouseMoveArgs(e);
    theInstance.invokeMethodAsync('OnMouseUp', args);
}

function mouseMove(e) {
    var args = canvasMouseMoveArgs(e);
    theInstance.invokeMethodAsync('OnMouseMove', args);
}

function mouseWheel(e) {
    var args = canvasMouseMoveArgs(e);
    theInstance.invokeMethodAsync('OnMouseWheel', args);
}

function getCanvasHolderSize() {
    var canvasHolder = $("#canvasHolder");
    var IDs = new Array();
    IDs[0] = 3;
    IDs[1] = 2;
    return true;
}

function canvasMouseMoveArgs(e) {
    return {
        ScreenX: e.screenX,
        ScreenY: e.screenY,
        ClientX: e.clientX,
        ClientY: e.clientY,
        MovementX: e.movementX,
        MovementY: e.movementY,
        OffsetX: e.offsetX,
        OffsetY: e.offsetY,
        AltKey: e.altKey,
        CtrlKey: e.ctrlKey,
        Button: e.button,
        Buttons: e.button,
        Bubbles: e.bubbles,
        ShiftKey: e.shiftKey,
        Delta: e.deltaY
        
    };
}