﻿using System.ComponentModel;
using System.Drawing;
using System.Numerics;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Simba.Client.Models;

namespace Simba.Client.Components
{
    public partial class CanvasHelper : ComponentBase, IAsyncDisposable
    {
        private Lazy<Task<IJSObjectReference>> moduleTask;
        private DateTime lastRender;

        [Inject]
        protected IJSRuntime _jsRuntime { get; set; }

        [Parameter]
        public EventCallback<Size> CanvasResized { get; set; }

        [Parameter]
        public EventCallback<double> RenderFrame { get; set; }

        [Parameter]
        public EventCallback<CanvasMouseArgs> MouseDown { get; set; }

        [Parameter]
        public EventCallback<CanvasMouseArgs> MouseUp { get; set; }

        [Parameter]
        public EventCallback<CanvasMouseArgs> MouseWheel { get; set; }

        [Parameter]
        public EventCallback<CanvasMouseArgs> MouseMove { get; set; }

        public async Task Initialize()
        {
            moduleTask = new(() => _jsRuntime.InvokeAsync<IJSObjectReference>("import", "./Components/CanvasHelper.razor.js").AsTask());
            var module = await moduleTask.Value;
            await module.InvokeVoidAsync("initRenderJS", DotNetObjectReference.Create(this));

            await module.DisposeAsync();
        }

        [JSInvokable]
        public async Task ResizeInBlazor(int width, int height)
        {
            var size = new Size(width, height);
            await CanvasResized.InvokeAsync(size);
        }

        [JSInvokable]
        public async ValueTask RenderInBlazor(float timeStamp)
        {
            double fps = 1.0 / (DateTime.Now - lastRender).TotalSeconds;
            lastRender = DateTime.Now; 
            await RenderFrame.InvokeAsync(fps);
        }

        [JSInvokable]
        public async Task OnMouseDown(CanvasMouseArgs args)
        {
            await MouseDown.InvokeAsync(args);
        }

        [JSInvokable]
        public async Task OnMouseUp(CanvasMouseArgs args)
        {
            await MouseUp.InvokeAsync(args);
        }

        [JSInvokable]
        public async Task OnMouseMove(CanvasMouseArgs args)
        {
            await MouseMove.InvokeAsync(args);
        }

        [JSInvokable]
        public async Task OnMouseWheel(CanvasMouseArgs args)
        {
            await MouseWheel.InvokeAsync(args);
        }

        public async Task<int[]> GetCanvasHolderSizeAsync()
        {
            var a = await _jsRuntime.InvokeAsync<bool>("getCanvasHolderSize");

            return new int[] { 1 };
        }

        public async ValueTask DisposeAsync()
        {
            if (moduleTask != null && moduleTask.IsValueCreated)
            {
                var module = await moduleTask.Value;
                await module.DisposeAsync();
            }
        }
    }
}
