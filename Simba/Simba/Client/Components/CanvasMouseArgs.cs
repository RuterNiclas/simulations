﻿namespace Simba.Client.Components
{
    public class CanvasMouseArgs
    {
        private static CanvasMouseArgsButtons canvasMouseArgsButtons = new CanvasMouseArgsButtons();
        public static CanvasMouseArgsButtons CanvasMouseArgsButtons => canvasMouseArgsButtons;

        public int ScreenX { get; set; }
        public int ScreenY { get; set; }
        public int ClientX { get; set; }
        public int ClientY { get; set; }
        public int MovementX { get; set; }
        public int MovementY { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public bool AltKey { get; set; }
        public bool CtrlKey { get; set; }
        public bool ShiftKey { get; set; }
        public bool Bubbles { get; set; }
        public int Buttons { get; set; }
        public int Button { get; set; }
        public int Delta { get; set; }
    }
}
