﻿namespace Simba.Client.Components
{
    public class CanvasMouseArgsButtons
    {
        public int LeftButton = 1;
        public int RightButton = 2;
    }
}
