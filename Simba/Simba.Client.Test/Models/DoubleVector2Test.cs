﻿using Simba.Client.Models;

namespace Simba.Client.Test.Models
{
	public class DoubleVector2Test
	{
		[Theory]
		[InlineData(0.3, 0.5, 0.3)]
		[InlineData(-0.3, 0.5, -0.3)]
		[InlineData(0.6, 0.5, 0.5)]
		[InlineData(-0.6, 0.5, -0.5)]
		[InlineData(-0.6, -0.5, null)]
		public void GetXAbsoulteLengthClampedBewlowOrEqual(double x, 
														   double clampLength, 
														   double? expectedValue)
		{
			if (expectedValue.HasValue)
			{
				var doubleVector2 = new DoubleVector2(x, 0);

				var clampValue = doubleVector2.GetXAbsoulteLengthClampedBewlowOrEqual(clampLength);

				Assert.Equal(expectedValue, clampValue);
			}
			else
			{
				try
				{
					var doubleVector2 = new DoubleVector2(x, 0);

					var clampValue = doubleVector2.GetXAbsoulteLengthClampedBewlowOrEqual(clampLength);

					Assert.True(false, "A negative clamp length is not ok. It shall throw.");
				}
				catch 
				{
				}
			}
		}

		[Theory]
		[InlineData(0.3, 0.5, 0.3)]
		[InlineData(-0.3, 0.5, -0.3)]
		[InlineData(0.6, 0.5, 0.5)]
		[InlineData(-0.6, 0.5, -0.5)]
		[InlineData(-0.6, -0.5, null)]
		public void GetYAbsoulteLengthClampedBewlowOrEqual(double y,
														   double clampLength,
														   double? expectedValue)
		{
			if (expectedValue.HasValue)
			{
				var doubleVector2 = new DoubleVector2(0, y);

				var clampValue = doubleVector2.GetYAbsoulteLengthClampedBewlowOrEqual(clampLength);

				Assert.Equal(expectedValue, clampValue);
			}
			else
			{
				try
				{
					var doubleVector2 = new DoubleVector2(y, 0);

					var clampValue = doubleVector2.GetYAbsoulteLengthClampedBewlowOrEqual(clampLength);

					Assert.True(false, "A negative clamp length is not ok. It shall throw.");
				}
				catch
				{
				}
			}
		}
	}
}
